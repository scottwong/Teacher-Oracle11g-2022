package com.scott;

import com.scott.bean.UserBean;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

//SpringBoot的Junit集成测试
@RunWith(SpringRunner.class)
//SpringBoot的测试环境初始化，参数：启动类
@SpringBootTest(classes = CrmApplication.class)
public class CrmApplicationTest {


    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Test
    public void findById(){

        //UserBean userBean = jdbcTemplate.queryForObject("select * from USER_T where id=10010", UserBean.class);
        //UserBean userBean = jdbcTemplate.queryForObject("select id,name,age from USER_T where id=10020", UserBean.class);
        //UserBean userBean = jdbcTemplate.queryForObject("select id,name,age from USER_T where id=10020", UserBean.class);

        System.out.println(jdbcTemplate.queryForObject("select id from USER_T where id=10020", BigDecimal.class));
        System.out.println(jdbcTemplate.queryForObject("select age from USER_T where id=10020", BigDecimal.class));
        System.out.println(jdbcTemplate.queryForObject("select name from USER_T where id=10020", String.class));

        // System.out.println(userBean);

    }

}
