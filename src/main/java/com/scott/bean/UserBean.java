package com.scott.bean;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class UserBean {

    private BigDecimal id;
    private String name;
    private BigDecimal age;

}
